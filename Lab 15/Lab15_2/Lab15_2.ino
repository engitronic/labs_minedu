/*Este programa permite controlar el encendido, pare (SERIAL) y 
cambio del sentido de giro de un motor(JOYSTICK)*/
int mota = 9;
int motb = 10;
int activa = 8;
int estado = 1;
int sentido = 1;
int val;
int vel;
String comando;
char dato;
void setup() {
    Serial.begin(9600);
    pinMode(mota,OUTPUT);
    pinMode(motb,OUTPUT); 
    pinMode(activa,OUTPUT);  
    digitalWrite(activa,LOW);
}
void loop() {
  val = analogRead(A0);
  if (Serial.available() > 0){
    dato = Serial.read();
    comando = comando + dato; 
    Serial.flush();
  }
  if(comando == "PRENDER"){
    digitalWrite(activa,HIGH);
    comando = "";
  }
  if(comando == "PARAR"){
    digitalWrite(activa,LOW);
    comando = "";
  }
  if(val >= 515){
    vel = map(val,515,1023,0,255);
    analogWrite(mota,vel);
    digitalWrite(motb,LOW);
  }
  if(val < 515){
    vel = map(val,0,516,255,0);
    analogWrite(motb,vel);
    digitalWrite(mota,LOW);
  }
}
