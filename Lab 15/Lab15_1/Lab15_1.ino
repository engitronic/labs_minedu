/*Este programa permite controlar el encendido, pare y cambio del
sentido de giro de un motor*/
int activa = 10;
int mota = 12;
int motb = 11;
int estado = 1;
int sentido = 1;
String comando;
char dato;
void setup() {
    Serial.begin(9600);
    pinMode(mota,OUTPUT);
    pinMode(motb,OUTPUT); 
    pinMode(activa,OUTPUT);  
    digitalWrite(activa,LOW);
    digitalWrite(mota,HIGH);
    digitalWrite(motb,LOW);
}

void loop() {
  if (Serial.available() > 0){
    dato = Serial.read();
    comando = comando + dato; 
    Serial.flush();
  }
  if(comando == "PRENDER"){
    digitalWrite(activa,HIGH);
    comando = "";
  }
  if(comando == "PARAR"){
    digitalWrite(activa,LOW);
    comando = "";
  }
  if(comando == "CAMBIAR"){
    if(estado == 1){
      sentido = 2;
    }
    if(estado == 2){
      sentido = 1;
    }
    if(sentido == 1){
      digitalWrite(mota,HIGH);
      digitalWrite(motb,LOW);
      estado = 1;
    }
    if(sentido == 2){
      digitalWrite(mota,LOW);
      digitalWrite(motb,HIGH);
      estado = 2;
    }
    comando = "";
  }  
}
