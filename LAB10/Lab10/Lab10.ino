//-----------------------------------------------------
//          "Verificador de intensidad de calor"
//-----------------------------------------------------
float val = 0;           // Estado variable de la lectura
void setup ()
{//Configurando
  Serial.begin(9600);    // Inicia el Serial, si se desea visualizar en el MONITOR SERIAL
  //Configurando pines de salida
  pinMode (13, OUTPUT) ;
  pinMode (12, OUTPUT) ;   
  pinMode (11, OUTPUT) ;
  pinMode (10, OUTPUT) ;  
  pinMode (9, OUTPUT) ;     
}

void loop ()
{//Instrucciones
  val = analogRead(A0);  // Carga el valor analogico leído
  // Formula para calcular la temperatura en grados Celsius
  val= val*0.48828125;   
  Serial.print(val); Serial.print(" grados Celsius \n");  // Imprimirlo
  //Comparaciones a diferentes temperaturas:
  if(val<25){
    digitalWrite (13, LOW);
    digitalWrite (12, LOW);
    digitalWrite (11, LOW);
    digitalWrite (10, LOW);
    digitalWrite (9, LOW);     
  }
  else if(val<37){
    digitalWrite (13, HIGH);
    digitalWrite (12, LOW);
    digitalWrite (11, LOW);
    digitalWrite (10, LOW);
    digitalWrite (9, LOW);    
  }
  else if(val<60){
    digitalWrite (13, HIGH);
    digitalWrite (12, HIGH);
    digitalWrite (11, LOW);
    digitalWrite (10, LOW);
    digitalWrite (9, LOW);    
  }
  else if(val<80){
    digitalWrite (13, HIGH);
    digitalWrite (12, HIGH);
    digitalWrite (11, HIGH);
    digitalWrite (10, LOW);
    digitalWrite (9, LOW);    
  }
  else if(val<100){
    digitalWrite (13, HIGH);
    digitalWrite (12, HIGH);
    digitalWrite (11, HIGH);
    digitalWrite (10, HIGH);
    digitalWrite (9, LOW);    
  }
  else{
    digitalWrite (13, HIGH);
    digitalWrite (12, HIGH);
    digitalWrite (11, HIGH);
    digitalWrite (10, HIGH);
    digitalWrite (9, HIGH);    
  }  
  delay(20);
}

