/* Este programa nos simulara el funcionamiento 
del semaforo 2 vias 
R --> 5s
A --> 1.5s
V --> 3.5s
*/
// Semaforo 1
int led_v = 7;               // Crea una variable local de valor 7
int led_a = 8;               // Crea una variable local de valor 8
int led_r = 9;               // Crea una variable local de valor 9
// Semaforo 2
int led2_v = 10;             // Crea una variable local de valor 10
int led2_a = 11;             // Crea una variable local de valor 11
int led2_r = 12;             // Crea una variable local de valor 12
void setup(){
  pinMode(led_v,OUTPUT);     // Configura el pin 7 como salida.
  pinMode(led_a,OUTPUT);     // Configura el pin 8 como salida.
  pinMode(led_r,OUTPUT);     // Configura el pin 9 como salida.
  pinMode(led2_v,OUTPUT);    // Configura el pin 10 como salida.
  pinMode(led2_a,OUTPUT);    // Configura el pin 11 como salida.
  pinMode(led2_r,OUTPUT);    // Configura el pin 12 como salida.
}
void loop(){
  digitalWrite(led_v,HIGH);  // Se prende el led v
  digitalWrite(led_a,LOW);   // Se apaga el led a
  digitalWrite(led_r,LOW);   // Se apaga el led r
  digitalWrite(led2_v,LOW);  // Se apaga el led v 2
  digitalWrite(led2_a,LOW);  // Se apaga el led a 2
  digitalWrite(led2_r,HIGH); // Se prende el led r 2
  delay(3500);               // Tiempo de espera de 3.5s
  digitalWrite(led_v,LOW);   // Se apaga el led v
  digitalWrite(led_a,HIGH);  // Se prende el led a
  digitalWrite(led_r,LOW);   // Se apaga el led r
  digitalWrite(led2_v,LOW);  // Se apaga el led v 2
  digitalWrite(led2_a,LOW);  // Se apaga el led a 2
  digitalWrite(led2_r,HIGH); // Se prende el led r 2
  delay(1500);               // Tiempo de espera de 1.5s
  digitalWrite(led_v,LOW);   // Se apaga el led v
  digitalWrite(led_a,LOW);   // Se apaga el led a
  digitalWrite(led_r,HIGH);  // Se prende el led r
  digitalWrite(led2_v,HIGH); // Se prende el led v 2
  digitalWrite(led2_a,LOW);  // Se apaga el led a 2
  digitalWrite(led2_r,LOW);  // Se apaga el led r 2
  delay(3500);               // Tiempo de espera de 3.5s
  digitalWrite(led_v,LOW);   // Se apaga el led v
  digitalWrite(led_a,LOW);   // Se apaga el led a
  digitalWrite(led_r,HIGH);  // Se prende el led r
  digitalWrite(led2_v,LOW);  // Se apaga el led v 2
  digitalWrite(led2_a,HIGH); // Se prende el led a 2
  digitalWrite(led2_r,LOW);  // Se apaga el led r 2
  delay(1500);               // Tiempo de espera de 1.5s
}
