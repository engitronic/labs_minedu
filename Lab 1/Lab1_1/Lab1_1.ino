/* Este programa nos simulara el funcionamiento 
del semaforo */
int led_v = 7;              // Crea una variable local de valor 7
int led_a = 8;              // Crea una variable local de valor 8
int led_r = 9;              // Crea una variable local de valor 9
void setup(){
  pinMode(led_v,OUTPUT);    // Configura el pin 7 como salida.
  pinMode(led_a,OUTPUT);    // Configura el pin 8 como salida.
  pinMode(led_r,OUTPUT);    // Configura el pin 9 como salida.
}
void loop(){
  digitalWrite(led_v,HIGH); // Se prende el led v
  digitalWrite(led_a,LOW);  // Se apaga el led a
  digitalWrite(led_r,LOW);  // Se apaga el led r
  delay(5000);              // Tiempo de espera de 5s
  digitalWrite(led_v,LOW);  // Se prende el led v
  digitalWrite(led_a,HIGH); // Se apaga el led a
  digitalWrite(led_r,LOW);  // Se apaga el led r
  delay(1500);              // Tiempo de espera de 5s
  digitalWrite(led_v,LOW);  // Se prende el led v
  digitalWrite(led_a,LOW);  // Se apaga el led a
  digitalWrite(led_r,HIGH); // Se apaga el led r
  delay(3500);              // Tiempo de espera de 5s
}
