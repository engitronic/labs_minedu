//-----------------------------------------------------
//       "Imprimir estados del LDR"
//-----------------------------------------------------
int LDR = 0;      //Valor Analogico del LDR 
void setup() {
  //Abrir puerto serial a 9600 baudios:
  Serial.begin(9600);
}

void loop() {
  //Leer el valor analogico:
  LDR = analogRead(0);

  // Imprimir Valor sensado:
  Serial.println("  Imprimiendo el valor analogico del LDR...");
  Serial.print("Valor en formato decimal      ");  
  Serial.println(LDR);       
  Serial.print("Valor en formato decimal      ");  
  Serial.println(LDR, DEC);  
  Serial.print("Valor en formato hexadecimal  ");  
  Serial.println(LDR, HEX); 
  Serial.print("Valor en formato octal        ");    
  Serial.println(LDR, OCT);  
  Serial.print("Valor en formato binario      ");  
  Serial.println(LDR, BIN);
  Serial.print("\n\n");      //Correr 2 lineas abajo
  //Retraso de 1seg:
  delay(1000);
}

