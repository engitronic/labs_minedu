//-----------------------------------------------------
//       "Imprimir “Hola Mundo” por Serial"
//-----------------------------------------------------
void setup() {
  //Abrir puerto serial a 9600 baudios:
  Serial.begin(9600);
  Serial.println("  Abriendo puerto serial...");
}

void loop() {
  //Imprimir "Hola mundo!":
  Serial.println("Hola mundo!");  
  //Retraso de 500ms
  delay(500);
}
