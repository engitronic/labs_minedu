//-----------------------------------------------------
//       "Cambio del juego de luces por Serial"
//-----------------------------------------------------

// Variable que almacena la lectura
int byteLeido; 

void setup()
{
  // Abriendo puerto Serial a 9600 baudios
  Serial.begin(9600);
  // Configurando salidas para los LEDs
  pinMode(2, OUTPUT);   
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
}

void loop()
{ // Si recibe datos
  if(Serial.available()>0){ 
        byteLeido = Serial.read()-48;  //Segun ASCII se debe realizar esta operacion
        Serial.println(byteLeido);     //Imprimir variable guardada
  }
  // Casos para valor leido
  if (byteLeido == 1){      //Cuando vale "1"
    digitalWrite(2, HIGH);
    digitalWrite(3, LOW);
    digitalWrite(4, HIGH);
    digitalWrite(5, LOW);
    digitalWrite(6, HIGH);
    delay(100);
    digitalWrite(2, LOW);
    digitalWrite(3, HIGH);
    digitalWrite(4, LOW);
    digitalWrite(5, HIGH);
    digitalWrite(6, LOW);
    delay(100);    
  }
  if (byteLeido == 2){      //Cuando vale "2"
    for(int i=2; i<7; i++){    
    digitalWrite(i, HIGH);delay(100);    
    }
    for(int i=6; i>1; i--){    
    digitalWrite(i, LOW);delay(100);    
    }    
  }
  if (byteLeido == 3){      //Cuando vale "3"
    for(int i=2; i<7; i++){    
    digitalWrite(i, LOW);delay(100);    
    }
    for(int i=6; i>1; i--){    
    digitalWrite(i, HIGH);delay(100);    
    }    
  }
  delay(50);  //Retardo de 50 ms para lectura
}
