//-----------------------------------------------------
//        "Introduccion de Pasword por Serial"
//-----------------------------------------------------

// Contraseña deseada, almacenada en un array
char password[]="MINEDU";   
// Variable que almacena la lectura
char byteLeido; 
// Variable que permite el acceso
int confirmacion;
int LED1 = 2;      // Pin del LED1
int LED2 = 3;      // Pin del LED2
int LED3 = 4;      // Pin del LED3
int LED4 = 5;      // Pin del LED4
int LED5 = 6;      // Pin del LED5
// La variable "LEDS" permite el encendido de los LEDs:
int LEDS = 0;  

void setup()
{
  // Abriendo puerto Serial a 9600 baudios
  Serial.begin(9600);
  // Configurando salidas para los LEDs
  pinMode(LED1, OUTPUT);   
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(LED4, OUTPUT);
  pinMode(LED5, OUTPUT);
}

void loop()
{ // Inicializa como si el password estuviera correcto 
  confirmacion = 1;
  // Si recibe datos
  if(Serial.available()>0){
    // Si los datos tienen la misma cantidad de letras que el password
    if(Serial.available() == sizeof(password)-1){
      // Lectura y almacenamiento de cada letra
      for (int i=0; i <sizeof(password)-1; i++){
        byteLeido = Serial.read();
        // Si no hay coinciedencia con las letras del password 
        if(byteLeido != password[i]){
          confirmacion=confirmacion*0;
        }        
      }
      // Si hubo completa coinciedencia, se imprime el mensaje
      if (confirmacion == 1){
        Serial.println("Bienvenido!!!");
        LEDS=1;               
      }
      else
      { // Si no hubo completa coinciedencia, se imprime el mensaje
        Serial.println("Password Incorrecto!");
        LEDS=0;
      }

    }
    // Si los datos no tienen la misma cantidad de letras que el password
    else
    { 
      LEDS=0;
      // Se descarta los datos leidos
      int trama=Serial.available();
      for (int i=0; i <trama; i++){
        byteLeido = Serial.read();
      }
      //Se imprime el mensaje
      Serial.println("Password Incorrecto!");     
    }     
  }
  // Si el password es correcto, encender
  if (LEDS == 1){
    digitalWrite(LED1, HIGH);
    digitalWrite(LED2, HIGH);
    digitalWrite(LED3, HIGH);
    digitalWrite(LED4, HIGH);
    digitalWrite(LED5, HIGH);    
  }
  else
  {// Si el password es incorrecto, apagar
    digitalWrite(LED1, LOW);
    digitalWrite(LED2, LOW);
    digitalWrite(LED3, LOW);
    digitalWrite(LED4, LOW);
    digitalWrite(LED5, LOW);   
  }  
  delay(50);  //Retardo de 50 ms para lectura
}


