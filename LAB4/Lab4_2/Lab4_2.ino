//-----------------------------------------------------
//    "Cambio de estados de juego de luces por LDR"
//-----------------------------------------------------
void setup(){
  pinMode(7,OUTPUT);          //Led 1
  pinMode(8,OUTPUT);          //Led 2
  pinMode(9,OUTPUT);          //Led 3
  pinMode(10,OUTPUT);         //Led 4
  pinMode(11,OUTPUT);         //Led 5
  pinMode(6,INPUT_PULLUP);    //Entrada del LDR
  Serial.begin(9600);
}
void loop(){
  if(digitalRead(6)== HIGH){       //Si el LDR cierra el circuito
    for(int i = 7;i<12;i++){       // Apaga los leds
      digitalWrite(i,LOW);   
    }
    for(int i = 7;i<12;i++){       // Prende de 7 a 11 los leds
      digitalWrite(i,HIGH);  
      delay(200);  
    }
  }
  else{                            //Si el LDR no cierra el circuito
    for(int i = 7;i<12;i++){       // Apaga los leds
      digitalWrite(i,LOW);    
    }
    for(int i = 11;i>6;i--){       // Prende de 11 al 7mo led
      digitalWrite(i,HIGH);
      delay(200);   
    }
  }  
  
}
