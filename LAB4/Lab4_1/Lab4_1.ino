//-----------------------------------------------------
//            "Sensor LDR activando LED"
//-----------------------------------------------------
int estadoLDR = 1;      //Inicializa como HIGH

void setup() {
  //Entrada digital del LDR
  pinMode(2, INPUT_PULLUP);
  //Salida digital para el LED
  pinMode(13, OUTPUT);
}

void loop() {
  //Lee el estado del LDR (hay luz -> 0, no hay luz ->1)
  estadoLDR=digitalRead(2);
  //Estado de salida del LED  
  digitalWrite(13, estadoLDR);

  delay(50);                            //Espera 50 milisegundos
}
