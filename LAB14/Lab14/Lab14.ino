//-----------------------------------------------------
//    "Habitacion inteligente con sensor en puerta"
//-----------------------------------------------------
int Led = 13 ;        // Pin del LED
int buttonpin = 3;    // Pin de la ENTRADA DIGITAL
int val = 0;          // Estado variable de la lectura

void setup ()
{//Configurando
  Serial.begin(9600);          // Inicia el Serial, si se desea visualizar en el MONITOR SERIAL
  pinMode (Led, OUTPUT) ;      // El pin del LED en modo SALIDA
  pinMode (buttonpin, INPUT) ; // output interface D0 is defined sensor
}

void loop ()
{//Instrucciones
  val = digitalRead(buttonpin);  // Carga el estado leído
  if (val == HIGH)               // ¿La puerta esta cerrada?
  {
    digitalWrite (Led, LOW);
  }  
  else{                         // ¿La puerta esta abierta?
    digitalWrite (Led, HIGH);
  }
}


