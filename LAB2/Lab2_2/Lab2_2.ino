//-----------------------------------------------------
//            "Cambio de sentido de Motor"
//-----------------------------------------------------
int pulMotor = 1;      //Inicializa como HIGH

void setup() {
  //Entrada digital para el motor (Configurado por defecto como HIGH)
  pinMode(13, INPUT_PULLUP);
  //Salida digital para un polo de motor
  pinMode(A0, OUTPUT);
  //Salida digital para el otro polo del motor
  pinMode(A1, OUTPUT);
}

void loop() {
  pulMotor = digitalRead(13);           //Lectura de pulsador de Motor
  digitalWrite(A0, !pulMotor);          //"+" o "-" en el polo del Motor
  digitalWrite(A1, pulMotor);           //"-" o "+" en el otro polo del Motor
  
  delay(50);                            //Espera 50 milisegundos
}
