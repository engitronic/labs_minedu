//-----------------------------------------------------
//                "Output LED y MOTOR"
//-----------------------------------------------------
int pulMotor = 1;      //Inicializa como HIGH
int pulLed = 1;        //Inicializa como HIGH

void setup() {
  //Entrada digital para el motor (Configurado por defecto como HIGH)
  pinMode(13, INPUT_PULLUP);
  //Salida digital para el motor
  pinMode(12, OUTPUT);
  //Entrada digital para el LED (Configurado por defecto como HIGH)
  pinMode(2, INPUT_PULLUP);
  //Salida digital para el LED
  pinMode(8, OUTPUT);
}

void loop() {
  pulMotor = digitalRead(13);           //Lectura de pulsador de Motor
  digitalWrite(12, !pulMotor);          //Activa o Desactiva el Motor
  
  pulLed = digitalRead(2);              //Lectura de pulsador de LED
  digitalWrite(8, !pulLed);             //Activa o Desactiva el LED
  
  delay(50);                            //Espera 50 milisegundos
}
