/*Este programa detecta cuando hay flama y prende un led rojo 
como alarma y cuando no hay nada un led verde.*/
int rojo = 6;
int verde = 7; 
int flama;                     // Variable para almacenar lectura
void setup(){
  pinMode(rojo,OUTPUT);        // Configura el pin como salida
  pinMode(verde,OUTPUT);       // Configura el pin como salida
}
void loop(){
  flama = analogRead(A0);      // Guarda la lectura del sensor
  if(flama < 995){ 
    digitalWrite(rojo,HIGH);
    digitalWrite(verde,LOW);
  }
  else{
    digitalWrite(verde,HIGH);
    digitalWrite(rojo,LOW);
  }
}
