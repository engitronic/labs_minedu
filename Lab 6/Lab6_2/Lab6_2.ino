/*Este programa detecta cuando hay flama y prende un motor
(como ventilador), mientra mas cerca ete la flama este girar{a 
más rapido.*/
int venb = 10;
int vena = 9;
int vel;
int flama;                     // Variable para almacenar lectura
void setup(){
  pinMode(vena,OUTPUT);       // Configura el pin como salida
  pinMode(venb,OUTPUT);        // Configura el pin como salida
}
void loop(){
  flama = analogRead(A0);      // Guarda la lectura del sensor
  if(flama > 1010){ 
    analogWrite(vena,0);
    digitalWrite(venb,LOW);
  }
  else{
    vel = map(flama,800,1010,255,0);
    analogWrite(vena,vel);
    digitalWrite(venb,LOW);
  }
  if(flama < 800){ 
    analogWrite(vena,255);
    digitalWrite(venb,LOW);
  }
  
}
