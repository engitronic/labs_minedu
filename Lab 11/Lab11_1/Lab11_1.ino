#include <dht11.h>       // Incluir Libreria DHT11
dht11 SENSOR;            // Creamos el objeto TEMPE
int DHT_out = 7;         // pin 7 como salida del sensor  
int lectura;
float temperatura;
float humedad;
void setup()
{
  Serial.begin(9600);
}

void loop()
{
  lectura = SENSOR.read(DHT_out);   //Leemos los datos del sensor
  //Mostramos la temperatura
  temperatura = SENSOR.temperature;
  humedad = SENSOR.humidity;
  Serial.print("Humedad: ");
  Serial.print(humedad);
  Serial.println(" %");
  Serial.print("Temperatura: ");
  Serial.print(temperatura);
  Serial.println(" C");
  delay(500); // pequeño retardo para una buena lectura
}
