/* En esta experiencia vamos a medir la distancia a la que se 
encuentra un objeto, la cual será impresa por el puerto serie
del Arduino */
int Trigger = 2;   // Pin digital 2 para el Trigger 
int Echo = 3;      // Pin digital 3 para el Echo 

void setup() {
  Serial.begin(9600);             // Iniciailzamos la comunicación
  pinMode(Trigger, OUTPUT);       // Pin como salida
  pinMode(Echo, INPUT);           // Pin como entrada
  digitalWrite(Trigger, LOW);
}

void loop()
{
  long tiempo;                    // Tiempo que demora en llegar al eco
  long distancia;                 // Distancia en centimetros
  digitalWrite(Trigger, HIGH);
  delayMicroseconds(10);          // Enviamos un pulso de 10us
  digitalWrite(Trigger, LOW);
  
  tiempo = pulseIn(Echo, HIGH);   // Obtenemos el ancho del pulso
  distancia = tiempo*0.017;       // Escalamos el tiempo a una distancia en cm
  
  Serial.print("Distancia: ");
  Serial.print(distancia);        // Enviamos por puerto serie el valor de la distancia
  Serial.println("cm");
  delay(100);                     // Esperamos 100ms
}
