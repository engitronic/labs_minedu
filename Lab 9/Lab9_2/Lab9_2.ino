#include <Servo.h>
Servo puerta;      // Creamos el objeto puerta para control del servo
int Trigger = 2;   // Pin digital 2 para el Trigger 
int Echo = 3;      // Pin digital 3 para el Echo 

void setup() {
  pinMode(Trigger, OUTPUT);       // Pin como salida
  pinMode(Echo, INPUT);           // Pin como entrada
  digitalWrite(Trigger, LOW);
  puerta.attach(9);
}

void loop()
{
  long tiempo;                    // Tiempo que demora en llegar al eco
  long distancia;                 // Distancia en centimetros
  digitalWrite(Trigger, HIGH);
  delayMicroseconds(10);          // Enviamos un pulso de 10us
  digitalWrite(Trigger, LOW);
  
  tiempo = pulseIn(Echo, HIGH);   // Obtenemos el ancho del pulso
  distancia = tiempo*0.017;       // Escalamos el tiempo a una distancia en cm
  if(distancia < 15){
    puerta.write(170);
    delay(15);
  }
  else{
    puerta.write(90);
    delay(15);
  }
}
