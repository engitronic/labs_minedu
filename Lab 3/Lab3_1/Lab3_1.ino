/* Este programa nos permitirá variar la cantidad de leds 
prendidos de acuerdo al valor del pótenciometro */
// Almacenar Valor del Pot
int valor;
// Para leds
int led1 = 7;              // Crea una variable local de valor 7
int led2 = 8;              // Crea una variable local de valor 8
int led3 = 9;              // Crea una variable local de valor 9
int led4 = 10;             // Crea una variable local de valor 10
int led5 = 11;             // Crea una variable local de valor 11
void setup(){
  pinMode(led1,OUTPUT);
  pinMode(led2,OUTPUT);
  pinMode(led3,OUTPUT);
  pinMode(led4,OUTPUT);
  pinMode(led5,OUTPUT);
}
void loop(){
  valor = analogRead(A0);          // Guarda la lectura (0-1023)
  if(valor < 204){                 // Prende 1 led
    digitalWrite(led1,HIGH);
    digitalWrite(led2,LOW);
    digitalWrite(led3,LOW);
    digitalWrite(led4,LOW);
    digitalWrite(led5,LOW);
  }
  else {
    if(valor < 409){              // Prende 2 leds  
      digitalWrite(led1,HIGH);
      digitalWrite(led2,HIGH);
      digitalWrite(led3,LOW);
      digitalWrite(led4,LOW);
      digitalWrite(led5,LOW);
    }
    else {
      if(valor < 613){            // Prende 3 leds
        digitalWrite(led1,HIGH);
        digitalWrite(led2,HIGH);
        digitalWrite(led3,HIGH);
        digitalWrite(led4,LOW);
        digitalWrite(led5,LOW);
      }
      else {
        if(valor < 818){          // Prende 4 leds
          digitalWrite(led1,HIGH);
          digitalWrite(led2,HIGH);
          digitalWrite(led3,HIGH);
          digitalWrite(led4,HIGH);
          digitalWrite(led5,LOW);
        }
        else{                     // Prende 5 leds
            digitalWrite(led1,HIGH);
            digitalWrite(led2,HIGH);
            digitalWrite(led3,HIGH);
            digitalWrite(led4,HIGH);
            digitalWrite(led5,HIGH);
        }
      }
    }
  }
}
