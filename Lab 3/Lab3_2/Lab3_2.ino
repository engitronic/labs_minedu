/* Este programa nos permitirá variar la velocidad del tiempo de 
parpadeo (100 -500ms) y con un pulsador cambiar el sentido de 
encendido de los leds*/
// Almacenar Valor del Pot
int valor;
// Para tiempo
int tiempo;
int pulsa = 6;
void setup(){
  pinMode(7,OUTPUT);
  pinMode(8,OUTPUT);
  pinMode(9,OUTPUT);
  pinMode(10,OUTPUT);
  pinMode(11,OUTPUT);
  pinMode(pulsa,INPUT_PULLUP);
  Serial.begin(9600);
}
void loop(){
  valor = analogRead(A0);          // Guarda la lectura (0-1023)
  if(valor < 204){                 // Tiempo 100ms
    tiempo = 100;
  }
  else {
    if(valor < 409){               // Tiempo 200ms
      tiempo = 200;
    }
    else {
      if(valor < 613){             // Tiempo 300ms
        tiempo = 300;
      }
      else {
        if(valor < 818){           // Tiempo 400ms
          tiempo = 400;
        }
        else{                      // Tiempo 500ms
            tiempo = 500;
        }
      }
    }
  }
  if(digitalRead(pulsa)== HIGH){
    for(int i = 7;i<12;i++){       // Apagan los leds
      digitalWrite(i,LOW);   
    }
    for(int i = 7;i<12;i++){       // Prende de 7 a 11 los leds
      digitalWrite(i,HIGH);  
      delay(tiempo);  
    }
  }
  else{
    for(int i = 7;i<12;i++){
      digitalWrite(i,LOW);    
    }
    for(int i = 11;i>6;i--){
      digitalWrite(i,HIGH);
      delay(tiempo);   
    }
  }  
  
}
