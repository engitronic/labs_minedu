/*Este programa permite controlar el sentido de gido de un motor
con un pulsador y vatiar la velocidad con un potenciometro*/
int mota = 12;
int motb = 11;
int sentido = 10;
int velocidad = 9;    
int valor;                     // Variable para almacenar lectura
void setup(){
  pinMode(mota,OUTPUT);        // Configura el pin como salida
  pinMode(motb,OUTPUT);        // Configura el pin como salida
  pinMode(sentido,INPUT_PULLUP);     // Configura el pin como salida
  pinMode(velocidad,OUTPUT);   // Configura el pin como salida
}
void loop(){
  valor = analogRead(A0);      // Guarda la lectura del potenciometro
  analogWrite(velocidad, valor/4);   // Hace variar la intensidad del led
  if(digitalRead(sentido)==1){
    digitalWrite(mota,HIGH);
    digitalWrite(motb,LOW);
  }
  else{
    digitalWrite(mota,LOW);
    digitalWrite(motb,HIGH);
  }
}

