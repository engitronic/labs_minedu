//-----------------------------------------------------
//                      "Ecualizador"
//-----------------------------------------------------
double val = 0;          // Estado variable de la lectura
//Encontrar el valor impreso por el Serial cuando no hay ruido.
int ref= 545; 
void setup ()
{//Configurando
  Serial.begin(9600);    // Inicia el Serial, si se desea visualizar en el MONITOR SERIAL
  //Configurando pines de salida
  pinMode (13, OUTPUT) ;
  pinMode (12, OUTPUT) ;   
  pinMode (11, OUTPUT) ;
  pinMode (10, OUTPUT) ;  
  pinMode (9, OUTPUT) ;     
}

void loop ()
{//Instrucciones
  val = analogRead(A0);  // Carga el valor analogico leído

  Serial.println(val);   // Imprimirlo
  //Comparaciones:
  if(val<ref){
    digitalWrite (13, LOW);
    digitalWrite (12, LOW);
    digitalWrite (11, LOW);
    digitalWrite (10, LOW);
    digitalWrite (9, LOW);     
  }
  else if(val<ref+5){
    digitalWrite (13, HIGH);
    digitalWrite (12, LOW);
    digitalWrite (11, LOW);
    digitalWrite (10, LOW);
    digitalWrite (9, LOW);    
  }
  else if(val<ref+10){
    digitalWrite (13, HIGH);
    digitalWrite (12, HIGH);
    digitalWrite (11, LOW);
    digitalWrite (10, LOW);
    digitalWrite (9, LOW);    
  }
  else if(val<ref+15){
    digitalWrite (13, HIGH);
    digitalWrite (12, HIGH);
    digitalWrite (11, HIGH);
    digitalWrite (10, LOW);
    digitalWrite (9, LOW);    
  }
  else if(val<ref+20){
    digitalWrite (13, HIGH);
    digitalWrite (12, HIGH);
    digitalWrite (11, HIGH);
    digitalWrite (10, HIGH);
    digitalWrite (9, LOW);    
  }
  else{
    digitalWrite (13, HIGH);
    digitalWrite (12, HIGH);
    digitalWrite (11, HIGH);
    digitalWrite (10, HIGH);
    digitalWrite (9, HIGH);    
  }  
  delay(20);
}

