//-----------------------------------------------------
//             "Chasqueos y manejo de Leds"
//-----------------------------------------------------
#include <TimerOne.h>  //Librería para INTERRUPCION TIMER

int Led = 13 ;        // Pin del LED
int buttonpin = 3;    // Pin de la ENTRADA DIGITAL
int val = 0;          // Estado variable de la lectura
int aplausos=0;       // Contador de aplausos

void setup ()
{//Configurando
  Serial.begin(9600);    // Inicia el Serial, si se desea visualizar en el MONITOR SERIAL
  pinMode (Led, OUTPUT) ;      // El pin del LED en modo SALIDA
  pinMode (buttonpin, INPUT) ; // output interface D0 is defined sensor
  Timer1.initialize(1300000);         // Dispara cada 1300 ms la interrupción TIMER
  Timer1.attachInterrupt(APLAUSOS);  // Activa la interrupción y la asocia a APLAUSOS 
}

void loop ()
{//Instrucciones
  val = digitalRead(buttonpin);  // Carga el estado leído
  if (val == HIGH)               // ¿Senso?
  {
    aplausos++;                  // Incrementa contador "aplauso"
    delay(60);                     // Pausa de 60 ms
  }
}

void APLAUSOS()
{   
  // El estado de "aplausos" es visualizable en el MONITOR SERIAL
  Serial.println(aplausos);  
  if (aplausos == 1){        // ¿Un aplauso?
    digitalWrite (Led, HIGH);     
  }
  if (aplausos > 1){         // ¿Dos aplausos?
    digitalWrite (Led, LOW);    
  }      
  aplausos=0;               // Reinicia contador "aplausos"
}


