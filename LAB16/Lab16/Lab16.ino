//-----------------------------------------------------
//   "Control de posicion de servomotor por joystick"
//-----------------------------------------------------

#include <Servo.h>   // Incluimos la libreria para servomotores
#include <math.h>    // Incluimos la libreria para matematicas
Servo miServo;       // Creando objeto de la libreria "Servo.h"

int joystickX = 5;  // Pin analogo del eje X
int joystickY = 4;  // Pin analogo del eje Y
double ejeX;        // Variable que almacena la posicion en el ejeX 
double ejeY;        // Variable que almacena la posicion en el ejeY 
double angulo;      // Almacena el angulo de giro del joystick
void setup() 
{ 
  miServo.attach(13);  // Pin del PWM usado para el servomotor 
  Serial.begin(9600);  // Inicia la comunicacion serial a 9600 baudios
  // Configuracion de pines de salida
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT); 
  pinMode(7, OUTPUT); 
} 

void loop() 
{ 
  ejeX = map(analogRead(joystickX), 0, 1023, 10, -10);     // Escala al ejeX de 10 a -10 
  ejeY = map(analogRead(joystickY), 0, 1023, -10, 10);     // Escala al ejeY de -10 a 10 
  // Calculo del angulo : tan-1(EjeY/EjeX) transformado a grados hexagesimales:
  angulo=atan2(ejeY,ejeX)*(180/3.141);

  //Imprimiendo en tiempo real  
  Serial.print("\n\n");
  Serial.print("ejeX: ");
  Serial.println(ejeX);
  Serial.print("ejeY: ");
  Serial.println(ejeY);
  Serial.print("Angulo: ");
  Serial.println(angulo);
  Serial.print("\n");
 
  if(angulo<-173){
    //Enciende 1 led
    digitalWrite(3, LOW);
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, LOW);
    digitalWrite(7, HIGH);  
  }
  else if(angulo<-135){
    //Enciende 2 leds
    digitalWrite(3, LOW);
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, HIGH);
    digitalWrite(7, HIGH);
  }
  else if(angulo<-90){
    //Enciende 3 leds
    digitalWrite(3, LOW);
    digitalWrite(4, LOW);
    digitalWrite(5, HIGH);
    digitalWrite(6, HIGH);
    digitalWrite(7, HIGH);
  }
  else if(angulo<-45){
    //Enciende 4 leds
    digitalWrite(3, LOW);
    digitalWrite(4, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, HIGH);
    digitalWrite(7, HIGH);
  }
  else if(angulo<0){    //<-5?
    //Enciende 5 leds
    digitalWrite(3, HIGH);
    digitalWrite(4, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, HIGH);
    digitalWrite(7, HIGH);
  }
  else if(angulo<5){
    //Apaga todos los leds
    digitalWrite(3, LOW);
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, LOW);
    digitalWrite(7, LOW);
  }
  else if(angulo<170){
    // Mueve el servomotor segun el angulo:
    miServo.write(angulo); 
    // Apaga todos los leds
    digitalWrite(3, LOW);
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, LOW);
    digitalWrite(7, LOW);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
  }
  else {
    /*No realizar accion, pues puede dañar 
    el servomotor si sobrepasa los 180 grados*/
  } 
  // Espera a que el servomotor llegue a la posicion indicada
  delay(15);    
} 

